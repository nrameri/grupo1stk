<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Eliminar afiliado</title>
</head>
<body>
  <div>
    <h1 align="center">¿Desea eliminar el afiliado?</h1>
  </div>
  <div align="left">
    <s:form method="post" theme="simple" action="eliminarAfiliado"  name="seleccionado" value="nro_afiliado" >
      <b>Nombre completo: </b><s:textfield value="%{campoNombre}" name="campoNombre" label="Nombre"  requiredLabel="true"  disabled="true"/>
      <br><br>
      <b>Tipo de Documento: </b><s:textfield value="%{tipoDocumentoCampo}" label="Tipo de documento" name="tipoDocumentoCampo"
        requiredLabel="true" disabled="true"/>
      <b>Numero de documento</b><s:textfield value="%{documentoCampo}" label="Numero de documento:" name="documentoCampo"
         requiredLabel="true" disabled="true"/>
      <br><br>
      <b>Plan Medico: </b><s:textfield value="%{planMedicoCampo.nombreplan}" label="Plan medico" name="planMedicoCampo"
         requiredLabel="true" disabled="true"/>
        <br><br>
      <b>Numero de afiliado: </b><s:textfield value="%{afiliadoCampo}" label="Numero de afiliado" name="afiliadoCampo"
         requiredLabel="true" disabled="true"/>
        <br><br>
      <s:submit value="Eliminar afiliado" /><%-- action="eliminarAfiliado" name="seleccionado" --%> 
    </s:form>
  </div>
</body>
</html>