<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>

	<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
	<link
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"
	rel="stylesheet" />

	<link rel="stylesheet" type="text/css"
	href="//cdn.datatables.net/1.10.11/css/jquery.dataTables.css">

	<script type="text/javascript" charset="utf8"
	src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.js"></script>

<title>Turnos</title>
</head>
<body>

	<link rel="stylesheet" type="text/css"
		href="https://cdn.datatables.net/v/dt/jq-3.2.1/dt-1.10.16/datatables.min.css" />

	<script type="text/javascript"
		src="https://cdn.datatables.net/v/dt/jq-3.2.1/dt-1.10.16/datatables.min.js"></script>

	<script>
		$(document).ready(function() {
			$('#tabla').dataTable();
		});
	</script>

	<title>Turnos</title>
</head>
<body>

	<!-- 	<div class="tab">
		<button class="tablinks" onclick="openCity(event, 'Afiliados')">Afiliados </button>
		<button class="tablinks" onclick="openCity(event, 'Turnos')">Turnos</button>
	</div> -->

	<div id="Afiliados" class="tabcontent">
		<br>
		<s:form action="tablaAfiliado" method="post"></s:form>
		<h1>Tabla Pacientes</h1>

		<br> <a href="<s:url action="paginaNuevoAfiliado"/>"><img
			src="resources-web/plus.png"></a> <a
			href="<s:url action="paginaEliminarAfiliado" />"><img
			src="resources-web/minus.png"></a> <a
			href="<s:url action="paginaModificarAfiliado" />"><img
			src="resources-web/reporte.png"></a>
		<table>

			<table cellspacing="0" id="tabla">
				<thead>
					<tr>
						<th><a href="#"><i class="material-icons"></i></a>Seleccionar</th>
						<th><a href="#"><i class="material-icons"></i></a> Nombre</th>
						<th><a href="#"><i class="material-icons"></i></a> Apellido</th>
						<th><a href="#"><i class="material-icons"></i></a> Doc_tipo</th>
						<th><a href="#"><i class="material-icons"></i></a> Documento</th>
						<th><a href="#"><i class="material-icons"></i></a> Direccion</th>
						<th><a href="#"><i class="material-icons"></i></a> Telefono</th>
						<th><a href="#"><i class="material-icons"></i></a> Email</th>
						<th><a href="#"><i class="material-icons"></i></a> Nacimiento</th>
					</tr>
				</thead>
				<tbody>
					<s:iterator value="listaAfiliados">
						<tr>
							<td><input type="radio" name="seleccionado"
								id="seleccionado" value="seleccionado"></td>
							<td><s:property value="nro_afiliado" /></td>
							<td><s:property value="plan.nombreplan" /></td>
							<td><s:property value="nombre_completo" /></td>
							<td><s:property value="tipodocu" /></td>
							<td><s:property value="documento" /></td>
							<td><s:property value="direccion" /></td>
							<td><s:property value="telefono" /></td>
							<td><s:property value="mail" /></td>
						</tr>
					</s:iterator>


					</div>
					<div id="Turnos" class="tabcontent"></div>
					<br>
					<s:form action="tablaTurnos" method="post"></s:form>
					<h1>Tabla Turnos</h1>

					<a href="<s:url action="paginaNuevoTurno" />"><img
						src="resources-web/plus.png"></a>
					<a href="<s:url action="paginaEliminarTurno" />"><img
						src="resources-web/minus.png"></a>
					<a href="<s:url action="paginaModificarTurno" />"><img
						src="resources-web/reporte.png"></a>
					<a href="<s:url action="paginaPDF" />"><img
						src="resources-web/pdf.png"></a>
					<table cellspacing="0" id="tabla">
						<thead>
							<tr>
								<th><a href="#"><i class="material-icons"></i></a>Numero
									turno</th>
								<th><a href="#"><i class="material-icons"></i></a> Dia
									turno</th>
								<th><a href="#"><i class="material-icons"></i></a> Horario</th>
								<th><a href="#"><i class="material-icons"></i></a> Practica</th>
								<th><a href="#"><i class="material-icons"></i></a>
									Prestador</th>
								<th><a href="#"><i class="material-icons"></i></a>
									Direccion</th>

							</tr>
						</thead>
						<tbody>
							<s:iterator value="listaTurnos">
								<tr>
									<td><input type="radio" name="seleccionado"
										id="seleccionado" value="seleccionado"></td>
									<td><s:property value="id_turno" /></td>
									<td><s:property value="fechaHora" /></td>
									<td><s:property value="practica.nombrepractica" /></td>
									<td><s:property value="prestador.nombre_completo" /></td>
									<td><s:property value="prestador.direccion" /></td>
								</tr>
								<!DOCTYPE html>


							</s:iterator>
					</table>


					</tr>
			</table>
</body>
</html>