<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="/struts-tags" prefix="s"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Eliminar turno</title>
</head>
<body>
	
<s:form method="post" theme="simple" action="eliminarTurno"  name="seleccionado" value="nro_afiliado" >
	<h1>�Desea eliminar el turno?</h1>
			<b>Afiliado: </b><s:textfield value="%{afiliado.nombre_completo}" label="Afiliado" name="afiliado" disabled="true"/>
					<br>
			<b>Prestador: </b><s:textfield value="%{prestador.nombre_completo}" label="Prestador" name="prestador" disabled="true"/>
					<br>
			<b>Practica: </b><s:textfield value="%{practica.nombrepractica}" label="Practica" name="practica" disabled="true"/>
					<br>
			<b>Fecha y hora: </b><s:textfield value="%{fechaHora}" label="Fecha" name="fechaHora" disabled="true"/>
					<br>
				<br>
			<s:submit value="Eliminar turno" action="eliminarTurno"  name="seleccionado"/>
		</s:form>
</body>
</html>