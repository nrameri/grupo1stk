<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="/struts-tags" prefix="s"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Nuevo turno</title>
</head>
<body>

<s:form action="nuevoTurno" method="post">
	<h1>Seleccione el nuevo turno</h1>
	
			<s:select label="Seleccione afiliado" name="codigoAfiliado"
				list="listaAfiliados" listKey="nro_afiliado" listValue="nombre_completo" />
					<br>
			<s:select label="Seleccione prestador" name="codigoPrestador"
				list="listaPrestadores" listKey="id_prestador" listValue="nombre_completo" />
					<br>
			<s:select label="Escoja practica" name="codigoPractica"
				list="listaPracticas" listKey="id_practica" listValue="nombrepractica"/>
				<br>
			<s:textfield label="Fecha de turno" name="campoFecha"
				placeholder="01/01/1978" />
				<br>
			<s:textfield label="Hora de turno" name="campoHora"
				placeholder="00:00" />
				<br>
			<s:submit value="Solicitar" />
		</s:form>
		
</body>
</html>