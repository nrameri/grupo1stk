<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Registro de Afiliado</title>
</head>
<body>
	<div>
		<h1 align="center">Ingrese los datos del nuevo afiliado</h1>
	</div>
	<div align="left">

		<s:form action="nuevoAfiliado" method="post" theme="simple">
			<b>Nombre completo: </b><s:textfield name="campoNombre" label="Nombre" placeholder="Nombres" />
			<br><br>
			<b>Tipo de Documento: </b><s:select label="Tipo de documento" name="tipoDocumentoCampo"
				list="{'Seleccione','DNI','Pasaporte','Libreta de enrolamiento','Cedula de identidad'}"
				/>
			<b>Nro de documento</b><s:textfield label="Numero de documento:" name="documentoCampo"
				placeholder="Numero" />
			<br><br>
			<b>Direccion: </b><s:textfield label="direccion" name="direccionCampo" placeholder="Calle altura departamento" />
				<br><br>
			<b>Telefono: </b><s:textfield label="Telefono" name="telefonoCampo"
				placeholder="Telefono" />
			<br><br>
			<b>Email</b><s:textfield label="Email" name="emailCampo" placeholder="@" />
				<br><br>
			<b>Fecha de nacimiento: </b><s:textfield label="Fecha de nacimiento" name="fechaCampo"
				placeholder="01/01/1978" />
				<br><br>
			<b>Sexo </b><s:select label="Sexo" name="sexoCampo"
				list="#{'M':'Masculino', 'F':'Femenino'}" />
				<br><br>
			<b>Estado Civil: </b><s:select label="Estado civil" name="estadoCivilCampo"
				list="{'Seleccione','Soltero/a','Casado/a','Viudo/a','Concubinato','Divorciado/a'}" />
				<br><br>
			<b>Hijos o Familiares a cargo: </b><s:textfield label="Hijos o familiares a cargo" name="hijosCampo"
				placeholder="N°" />
				<br><br>
			<b>Plan Medico: </b><s:select name="codigoPlan"
				list="listaPlanes" listKey="id_plan" listValue="nombreplan" />
				<br><br>
			
			<s:submit value="Enviar" /> 
		</s:form>
	</div>
</body>
</html>