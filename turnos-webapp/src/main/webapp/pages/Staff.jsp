<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
  pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
    <link rel="stylesheet" href="estilos/estilos1.css">
    <script type="text/javascript" src="jscripts/scripts1.js"></script>
    <title>Turnos</title>
  </head>
<body>
  
  <div class="tab">
    <button class="tablinks" onclick="openCity(event, 'Afiliados')">Afiliados </button>
    <button class="tablinks" onclick="openCity(event, 'Turnos')">Turnos</button>
  </div>
  
  <div id="Afiliados" class="tabcontent">
    <br>
    <s:form action="tablaAfiliado" method="post">
    <h1> Tabla Pacientes</h1>
    
    <br>
    
    <a href="<s:url action="paginaNuevoAfiliado"/>" ><img src="resources-web/plus.png"></a>
    
    <table> 
        <tr>
            <th>Seleccionar</td>
            <th>N. Afiliado</th>
            <th>Plan</th>
            <th>Nombre y apellido</td>
            <th>Tipo</td>
	        <th>Nro documento</td>
	        <th>Direccion</td>
	        <th>Telefono</td>
	        <th>Mail</td>
        </tr>
        
     	<s:iterator value="listaAfiliados">
	        <tr>
		          <td> 
				        <a href="<s:url action="paginaEliminarAfiliado"><s:param name="seleccionado"><s:property value="nro_afiliado"/></s:param></s:url>" ><img src="resources-web/minus.png"></a>
				        <a href="<s:url action="paginaModificarAfiliado"><s:param name="seleccionado"><s:property value="nro_afiliado"/></s:param></s:url>" ><img src="resources-web/reporte.png"></a>
		          </td>
		          
		          <td><s:property value="nro_afiliado"/></td>
		          <td><s:property value="plan.nombreplan"/></td>  
		          <td><s:property value="nombre_completo"/></td>
		          <td><s:property value="tipodocu"/></td>
		          <td><s:property value="documento"/></td>
		          <td><s:property value="direccion"/></td>
		          <td><s:property value="telefono"/></td>  
		          <td><s:property value="mail"/></td>
	       	</tr>
   		</s:iterator>
 
    </s:form>
  </div>
  
  <div id="Turnos" class="tabcontent">
    <br>
    <s:form action="tablaTurnos" method="post">
    <h1>Tabla Turnos</h1>
    
    <a href="<s:url action="paginaNuevoTurno" />"><s:param name="seleccionado"><s:property value="nro_afiliado"/></s:param><img src="resources-web/plus.png"></a>
    <a href="<s:url action="paginaPDF" />"><img src="resources-web/pdf.png"></a>
     
     <table> 
        <tr>
          <th>Seleccionar</th>
            <th>Nro. turno</th>
            <th>Dia y horario</th>
            <th>Afiliado</th>
            <th>Practica</th>
            <th>Prestador</th>
            <th>Direccion</th>
            
        </tr>
           <s:iterator value="listaTurnos">
          <tr>
            <td> 
        <a href="<s:url action="paginaEliminarTurno"><s:param name="seleccionado"><s:property value="id_turno"/></s:param></s:url>" ><img src="resources-web/minus.png"></a>
        <a href="<s:url action="paginaModificarTurno"><s:param name="seleccionado"><s:property value="nro_afiliado"/></s:param></s:url>" ><img src="resources-web/reporte.png"></a>
          </td>   
              <td><s:property value="id_turno"/></td>
              <td><s:property value="fechaHora"/></td>
              <td><s:property value="afiliado.nombre_completo"/></td>
              <td><s:property value="practica.nombrepractica"/></td>
              <td><s:property value="prestador.nombre_completo"/></td>
              <td><s:property value="prestador.direccion"/></td> 
          </tr>
      </s:iterator>
    </table>
 
    </s:form>
  </div>