package com.softtek.academia.actions;

import java.util.List;

import com.opensymphony.xwork2.ActionSupport;
import com.softtek.academia.bo.AfiliadoBO;
import com.softtek.academia.bo.LoginBO;
import com.softtek.academia.bo.TurnoBO;
import com.softtek.academia.entities.Afiliado;
import com.softtek.academia.entities.Turno;

public class LoginAction extends ActionSupport {
	private boolean acceso;
	
	private String usuario;
	private String contrasena;
	private LoginBO log;
	
	private List<Afiliado> listaAfiliados;
	private List<Turno> listaTurnos;
	
	private AfiliadoBO afi;
	private TurnoBO turnoBO;

	//GETTERS Y SETTERS
	public boolean getAcceso() {
		return acceso;
	}
	public void setAcceso(boolean acceso) {
		this.acceso = acceso;
	}
	public void setLog(LoginBO log) {
		this.log = log;
	}
	public LoginBO getLog() {
		return log;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setContrasena(String contrasena) {
		this.contrasena = contrasena;
	}
	public String getContrasena() {
		return contrasena;
	}
	public List<Afiliado> getListaAfiliados() {
		return listaAfiliados;
	}
	public void setListaAfiliados(List<Afiliado> listaAfiliados) {
		this.listaAfiliados = listaAfiliados;
	}
	public AfiliadoBO getAfi() {
		return afi;
	}
	public void setAfi(AfiliadoBO afi) {
		this.afi = afi;
	}
	
	public List<Turno> getListaTurnos() {
		return listaTurnos;
	}
	public void setListaTurnos(List<Turno> listaTurnos) {
		this.listaTurnos = listaTurnos;
	}
	public TurnoBO getTurnoBO() {
		return turnoBO;
	}
	public void setTurnoBO(TurnoBO turnoBO) {
		this.turnoBO = turnoBO;
	}
	
	//METODO
	public String execute() {
		try {
			this.acceso = this.log.acceder(usuario, contrasena);
			this.listaAfiliados = this.afi.obtenerAfiliadoGeneral();
			this.listaTurnos = this.turnoBO.obtenerTurnos();
		} catch (Exception e) {
			System.out.println("Error al acceder!");
		}
		if (acceso == true) {
			return SUCCESS;
		} else {
			return ERROR;
		}
	}
}
