package com.softtek.academia.actions;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.opensymphony.xwork2.ActionSupport;
import com.softtek.academia.bo.AfiliadoBO;
import com.softtek.academia.bo.PracticaBO;
import com.softtek.academia.bo.PrestadorBO;
import com.softtek.academia.bo.TurnoBO;
import com.softtek.academia.entities.Afiliado;
import com.softtek.academia.entities.Practica;
import com.softtek.academia.entities.Prestador;
import com.softtek.academia.entities.Turno;
import java.util.List;

public class TurnoAction extends ActionSupport {

	private int id_turno;
	private Date fechaHora;
	private Afiliado afiliado;
	private Prestador prestador;
	private Practica practica;
	private float importe;
	private int duracion;

	private boolean registro;
	private TurnoBO turnoBO;
	private List<Turno> listaTurnos;
	private Turno turno;
	private int codigoPrestador;
	private int codigoPractica;
	private int codigoAfiliado;
	private PrestadorBO prestadorBO;
	private PracticaBO practicaBO;
	private AfiliadoBO afiliadoBO;
	private List<Prestador> listaPrestadores;
	private List<Practica> listaPracticas;
	private List<Afiliado> listaAfiliados;
	private String campoFecha;
	private String campoHora;
	private List<Afiliado>listaAfiliados;
	private int seleccionado;
	private AfiliadoBO afi;
	//Constructor
	public TurnoAction() {
		
	}

	
	public AfiliadoBO getAfi() {
		return afi;
	}
	public void setAfi(AfiliadoBO afi) {
		this.afi = afi;
	}
	public List<Afiliado> getListaAfiliados() {
		return listaAfiliados;
	}
	public void setListaAfiliados(List<Afiliado> listaAfiliados) {
		this.listaAfiliados = listaAfiliados;
	}
	public String getCampoFecha() {
		return campoFecha;
	}
	public void setCampoFecha(String campoFecha) {
		this.campoFecha = campoFecha;
	}
	public String getCampoHora() {
		return campoHora;
	}
	public void setCampoHora(String campoHora) {
		this.campoHora = campoHora;
	}
	public int getCodigoPractica() {
		return codigoPractica;
	}
	public void setCodigoPractica(int codigoPractica) {
		this.codigoPractica = codigoPractica;
	}
	public int getCodigoPrestador() {
		return codigoPrestador;
	}
	public void setCodigoPrestador(int codigoPrestador) {
		this.codigoPrestador = codigoPrestador;
	}
	public Turno getTurno() {
		return turno;
	}
	public void setTurno(Turno turno) {
		this.turno = turno;
	}

	// GETTERS Y SETTERS
	public int getId_turno() {
		return id_turno;
	}
	public void setId_turno(int id_turno) {
		this.id_turno = id_turno;
	}
	public Date getFechaHora() {
		return fechaHora;
	}
	public void setFechaHora(Date fechaHora) {
		this.fechaHora = fechaHora;
	}
	public Afiliado getAfiliado() {
		return afiliado;
	}
	public void setAfiliado(Afiliado afiliado) {
		this.afiliado = afiliado;
	}
	public Prestador getPrestador() {
		return prestador;
	}
	public void setPrestador(Prestador prestador) {
		this.prestador = prestador;
	}
	public Practica getPractica() {
		return practica;
	}
	public void setPractica(Practica practica) {
		this.practica = practica;
	}
	public float getImporte() {
		return importe;
	}
	public void setImporte(float importe) {
		this.importe = importe;
	}
	public int getDuracion() {
		return duracion;
	}

	public void setDuracion(int duracion) {
		this.duracion = duracion;
	}

	public TurnoBO getTurnoBO() {
		return turnoBO;
	}

	public void setTurnoBO(TurnoBO turnoBO) {
		this.turnoBO = turnoBO;
	}

	public List<Turno> getListaTurnos() {
		return listaTurnos;
	}

	public void setListaTurnos(List<Turno> listaTurnos) {
		this.listaTurnos = listaTurnos;
	}

	public boolean isRegistro() {
		return registro;
	}

	public void setRegistro(boolean registro) {
		this.registro = registro;
	}

	public PrestadorBO getPrestadorBO() {
		return prestadorBO;
	}

	public void setPrestadorBO(PrestadorBO prestadorBO) {
		this.prestadorBO = prestadorBO;
	}

	public PracticaBO getPracticaBO() {
		return practicaBO;
	}

	public void setPracticaBO(PracticaBO practicaBO) {
		this.practicaBO = practicaBO;
	}

	public List<Prestador> getListaPrestadores() {
		return listaPrestadores;
	}

	public void setListaPrestadores(List<Prestador> listaPrestadores) {
		this.listaPrestadores = listaPrestadores;
	}

	public List<Practica> getListaPracticas() {
		return listaPracticas;
	}

	public void setListaPracticas(List<Practica> listaPracticas) {
		this.listaPracticas = listaPracticas;
	}

	public List<Afiliado> getListaAfiliados() {
		return listaAfiliados;
	}
	public void setListaAfiliados(List<Afiliado> listaAfiliados) {
		this.listaAfiliados = listaAfiliados;
	}
	public int getCodigoAfiliado() {
		return codigoAfiliado;
	}
	public void setCodigoAfiliado(int codigoAfiliado) {
		this.codigoAfiliado = codigoAfiliado;
	}
	public AfiliadoBO getAfiliadoBO() {
		return afiliadoBO;
	}
	public void setAfiliadoBO(AfiliadoBO afiliadoBO) {
		this.afiliadoBO = afiliadoBO;
	}
	
	public int getSeleccionado() {
		return seleccionado;
	}
	public void setSeleccionado(int seleccionado) {
		this.seleccionado = seleccionado;
	}

	// BUSQUEDA LISTAS
	public String tablaTurnos() {
		listaTurnos = turnoBO.obtenerTurnos();
		return SUCCESS;
	}
	public String busquedaPracticas() {
		listaPracticas = practicaBO.obtenerPracticas();
		return SUCCESS;
	}

	public String busquedaPrestadores(){
		listaPrestadores = prestadorBO.obtenerPrestadores();
		return SUCCESS;
	}
	
	public String busquedaAfiliados(){
		listaAfiliados = afiliadoBO.obtenerAfiliadoGeneral();
		System.out.println(listaAfiliados);
		return SUCCESS;
	}

	// METODO CAMBIO PAGINA NUEVO TURNO Y CARGA LISTAS DE SELECT
	public String paginaNuevoTurno() {
		busquedaPrestadores();
		busquedaPracticas();
		busquedaAfiliados();
		return SUCCESS;
	}

	// CAMBIO PAGINA A MODIFICAR TURNO
	public String paginaModificarTurno() {

		return SUCCESS;
	}


	// CAMBIO PAGINA PDF
	public String paginaPDF() {
		this.setListaAfiliados(this.afi.obtenerAfiliadoGeneral());
		this.listaTurnos = this.turnoBO.obtenerTurnos();
		return SUCCESS;
	}


	//CREA TURNO --PROBANDO
	public String nuevoTurno() {
		Turno t = null;
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
		try {
			t = new Turno();
			t.setId_turno(obtenerNumero());
			System.out.println(campoFecha);
			t.setFechaHora(dateFormat.parse(campoFecha +" "+ campoHora));
			t.setAfiliado(listaAfiliados.get(codigoAfiliado + 1));
			t.setPrestador(listaPrestadores.get(codigoPrestador + 1));
			t.setPractica(listaPracticas.get(codigoPractica + 1));
			t.setImporte(importe + 10);
			t.setDuracion(duracion + 10);
		}catch (Exception e) {
			System.out.println("Error al crear objeto Turno");
		}
		boolean registro;
		try{
			registro = this.turnoBO.crearTurno(t);
		}catch(Exception e){
			registro = false;
		}
		System.out.println(t);
		System.out.println(registro);
		if(registro){
			this.setListaAfiliados(this.afi.obtenerAfiliadoGeneral());
			this.listaTurnos = this.turnoBO.obtenerTurnos();
			return SUCCESS;
		}else{
			return ERROR;
		}
	}
	//CAMBIO PAGINA A ELIMINAR TURNO
	public String paginaEliminarTurno() {
		turno = turnoBO.busquedaPorCodigo(seleccionado);
		System.out.println(seleccionado);
		System.out.println(turno);
		afiliado = turno.getAfiliado();
		practica = turno.getPractica();
		prestador = turno.getPrestador();
		fechaHora = turno.getFechaHora();
		return SUCCESS;
	}

	// ELIMINA EL TURNO DE LA BASE DE DATOS
	public String eliminarTurno() {

		System.out.println(seleccionado + "llega");
		turno = turnoBO.busquedaPorCodigo(seleccionado);
		System.out.println(turno);
		
		boolean registro = turnoBO.eliminarTurno(turno);
		
		if (registro == true) {
			System.out.println("Se elimino el turno action");
			this.setListaAfiliados(this.afi.obtenerAfiliadoGeneral());
			this.listaTurnos = this.turnoBO.obtenerTurnos();
			return SUCCESS;
		} else
			System.out.println("No se eliminaaaaaa turno ACTION");
		return ERROR;
	}
	
	

	private Afiliado afiTurno() {
		return null;
	}
	// obtiene numero aleatorio para el turno
 		private int obtenerNumero() {
			int x;
			do {
				x = (int) (Math.random() * 9999 + 1);
			} while (this.turnoBO.busquedaPorCodigo(x) != null);
			return x;
		}
	// ALTA BAJA MODIFICACION TURNOS
	/*public String nuevoTurno() {
		registro = this.turnoBO.crearTurno(fechaHora, afiliado, prestador, practica, importe, duracion);
		if (registro = true) {
			return SUCCESS;
		} else
			return ERROR;
	}*/

	public String modificarTurno() {
		registro = turnoBO.modificarTurno(id_turno, fechaHora, afiliado, prestador, practica, importe, duracion);
		if (registro = true) {
			this.setListaAfiliados(this.afi.obtenerAfiliadoGeneral());
			this.listaTurnos = this.turnoBO.obtenerTurnos();
			return SUCCESS;
		} else {
			return ERROR;
		}
	}

	
}
