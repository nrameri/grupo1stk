package com.softtek.academia.actions;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.opensymphony.xwork2.ActionSupport;
import com.softtek.academia.bo.AfiliadoBO;
import com.softtek.academia.bo.PlanBO;
import com.softtek.academia.bo.TurnoBO;
import com.softtek.academia.entities.Afiliado;
import com.softtek.academia.entities.Plan;
import com.softtek.academia.entities.Turno;

public class AfiliadoAction extends ActionSupport {

	// Variables
	private int afiliadoCampo; // nro afiliado
	private Plan planMedicoCampo;
	private TurnoBO turnoBO;
	private String campoNombre;
	private String tipoDocumentoCampo;
	private String documentoCampo;
	private String direccionCampo;
	private int telefonoCampo;
	private String emailCampo;
	private Date fechaCampo;
	private String sexoCampo;
	private String estadoCivilCampo;
	private int hijosCampo;
	private int codigoPlan;
	private String estado;

	private int seleccionado; // trae el numero de afiliado

	private boolean exito;
	private AfiliadoBO afi;
	private PlanBO pl;
	private Afiliado afiliado;
	private List<Afiliado> listaAfiliados;
	private List<Turno> listaTurnos;
	private List<Plan> listaPlanes;

	// Constructor
	public AfiliadoAction() {
	}


	// GETTERS
	

	public TurnoBO getTurnoBO() {
		return turnoBO;
	}
	public void setTurnoBO(TurnoBO turnoBO) {
		this.turnoBO = turnoBO;
	}
	public int getCodigoPlan() {
		return codigoPlan;
	}
	public Afiliado getAfiliado() {
		return afiliado;
	}
	public List<Turno> getListaTurnos() {
		return listaTurnos;
	}
	public void setListaTurnos(List<Turno> listaTurnos) {
		this.listaTurnos = listaTurnos;
	}
	public String getCampoNombre() {
		return campoNombre;
	}

	public String getTipoDocumentoCampo() {
		return tipoDocumentoCampo;
	}
	public String getDocumentoCampo() {
		return documentoCampo;
	}
	public String getDireccionCampo() {
		return direccionCampo;
	}
	public int getTelefonoCampo() {
		return telefonoCampo;
	}
	public String getEmailCampo() {
		return emailCampo;
	}
	public Date getFechaCampo() {
		return fechaCampo;
	}
	public String getSexoCampo() {
		return sexoCampo;
	}
	public String getEstadoCivilCampo() {
		return estadoCivilCampo;
	}
	public int getHijosCampo() {
		return hijosCampo;
	}
	public Plan getPlanMedicoCampo() {
		return planMedicoCampo;
	}
	public int getAfiliadoCampo() {
		return afiliadoCampo;
	}
	public boolean isExito() {
		return exito;
	}
	public AfiliadoBO getAfi() {
		return afi;
	}
	public PlanBO getPl() {
		return pl;
	}
	public List<Afiliado> getListaAfiliados() {
		return listaAfiliados;
	}
	public List<Plan> getListaPlanes() {
		return listaPlanes;
	}
	public int getSeleccionado() {
		return seleccionado;
	}
	public String getEstado() {
		return estado;
	}

	// SETTERS
	public void setCampoNombre(String campoNombre) {
		this.campoNombre = campoNombre;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public void setTipoDocumentoCampo(String tipoDocumentoCampo) {
		this.tipoDocumentoCampo = tipoDocumentoCampo;
	}
	public void setCodigoPlan(int codigoPlan) {
		this.codigoPlan = codigoPlan;
	}
	public void setDocumentoCampo(String documentoCampo) {
		this.documentoCampo = documentoCampo;
	}
	public void setAfiliado(Afiliado afiliado) {
		this.afiliado = afiliado;
	}
	public void setDireccionCampo(String direccionCampo) {
		this.direccionCampo = direccionCampo;
	}
	public void setTelefonoCampo(int telefonoCampo) {
		this.telefonoCampo = telefonoCampo;
	}

	public void setEmailCampo(String emailCampo) {
		this.emailCampo = emailCampo;
	}
	public void setFechaCampo(Date fechaCampo) {
		this.fechaCampo = fechaCampo;
	}
	public void setSexoCampo(String sexoCampo) {
		this.sexoCampo = sexoCampo;
	}
	public void setEstadoCivilCampo(String estadoCivilCampo) {
		this.estadoCivilCampo = estadoCivilCampo;
	}
	public void setHijosCampo(int hijosCampo) {
		this.hijosCampo = hijosCampo;
	}
	public void setPlanMedicoCampo(Plan planMedicoCampo) {
		this.planMedicoCampo = planMedicoCampo;
	}
	public void setAfiliadoCampo(int afiliadoCampo) {
		this.afiliadoCampo = afiliadoCampo;
	}
	public void setExito(boolean exito) {
		this.exito = exito;
	}
	public void setAfi(AfiliadoBO afi) {
		this.afi = afi;
	}
	public void setPl(PlanBO pl) {
		this.pl = pl;
	}
	public void setListaAfiliados(List<Afiliado> listaAfiliados) {
		this.listaAfiliados = listaAfiliados;
	}

	
	public void setListaPlanes(List<Plan> listaPlanes) {
		try {
			this.listaPlanes = listaPlanes;
		} catch (Exception e) {
			System.out.println("Error en set");
		}	 
	}
	
	public void setSeleccionado(int seleccionado) { 
	    this.seleccionado = seleccionado;
	}
		 
	// TO STRING
	@Override
	public String toString() {
		return "AfiliadoAction [campoNombre=" + campoNombre + ", tipoDocumentoCampo=" + tipoDocumentoCampo
				+ ", documentoCampo=" + documentoCampo + ", direccionCampo=" + direccionCampo + ", telefonoCampo="
				+ telefonoCampo + ", emailCampo=" + emailCampo + ", fechaCampo=" + fechaCampo + ", sexoCampo="
				+ sexoCampo + ", estadoCivilCampo=" + estadoCivilCampo + ", hijosCampo=" + hijosCampo
				+ ", planMedicoCampo=" + planMedicoCampo + ", afiliadoCampo=" + afiliadoCampo + ", estado=" + estado
				+ ", exito=" + exito + ", afi=" + afi + ", pl=" + pl + ", listaAfiliados=" + listaAfiliados
				+ ", listaPlanes=" + listaPlanes + "]";
	}

	
<<<<<<< HEAD
	
	// METODOS EXECUTE
=======
	//METODOS EXECUTE
	

>>>>>>> master
	public String crearAfiliado(){
		Afiliado afiliado = new Afiliado();
			afiliado.setNombre_completo(campoNombre);
			afiliado.setTipodocu(tipoDocumentoCampo);
			afiliado.setDocumento(documentoCampo);
			afiliado.setDireccion(direccionCampo);
			afiliado.setTelefono(telefonoCampo);
			afiliado.setMail(emailCampo);
			afiliado.setFecha_nacimiento(fechaCampo);
			afiliado.setSexo(sexoCampo);
			afiliado.setEstado_civil(estadoCivilCampo);
			afiliado.setFamiliar_a_cargo(hijosCampo);
			afiliado.setNro_afiliado(obtenerNumero());
			afiliado.setPlan(listaPlanes.get(codigoPlan - 1));
			afiliado.setEstado("1");
			
			
		boolean registro;
		try{
			registro = this.afi.addAfiliado(afiliado);
		}catch(Exception e){
			registro = false;
		}

		if(registro){
			this.listaAfiliados = this.afi.obtenerAfiliadoGeneral();
			this.listaTurnos = this.turnoBO.obtenerTurnos();
			return SUCCESS;
		}else{
			
			return ERROR;
		}
	}

	// BUSQUEDA AFILIADOS Y PLANES
	public String tablaAfiliados() {
		listaAfiliados = afi.obtenerAfiliadoGeneral();
		return SUCCESS;
	}

	// METODOS CAMBIO PAGINA NUEVO AFILIADO
	public String paginaNuevoAfiliado() {
		buscarPlanes();
		return SUCCESS;
	}

	// METODOS CAMBIO PAGINA MODIFICAR AFILIADO
	public String paginaModificarAfiliado() {
		buscarPlanes();
		return SUCCESS;
	}

	// METODOS CAMBIO PAGINA ELIMINAR AFILIADO
	public String paginaEliminarAfiliado() {
		
		
		afiliado = afi.busquedaPorCodigo(seleccionado);
		/*System.out.println(seleccionado);
		System.out.println(afiliado);*/
		campoNombre = afiliado.getNombre_completo();
		afiliadoCampo = afiliado.getNro_afiliado();
		documentoCampo = afiliado.getDocumento();
		tipoDocumentoCampo = afiliado.getTipodocu();
		planMedicoCampo = afiliado.getPlan();
		
		return SUCCESS;

	}

	// ELIMINA EL AFILIADO DE LA BASE DE DATOS
	public String eliminarAfiliado() {

		//System.out.println(seleccionado + "llega");
		afiliado = afi.busquedaPorCodigo(seleccionado);
		//System.out.println(afiliado);
		this.listaAfiliados = this.afi.obtenerAfiliadoGeneral();
		this.listaTurnos = this.turnoBO.obtenerTurnos();
		
		boolean registro = afi.eliminarAfiliado(afiliado);
		
		if (registro == true) {
			//System.out.println("Se elimino el afiliado");
			return SUCCESS;
		} else {
			//System.out.println("No se eliminaaaaaa ACTION");
			return ERROR;
		}
	}
	//MODIFICACION AFILIADO
	public String modificarAfiliado() {
		System.out.println(seleccionado + "llega");
		afiliado = afi.busquedaPorCodigo(seleccionado);
		System.out.println(afiliado);
		this.listaAfiliados = this.afi.obtenerAfiliadoGeneral();
		this.listaTurnos = this.turnoBO.obtenerTurnos();
		
		boolean registro = afi.modificarAfiliado(afiliado);
		
		if(registro == true) {
			System.out.println("se modifico afiliado");
			return SUCCESS;
		}else {
			System.out.println("error al modificar");
			return ERROR;
		}
	}
	
	public String buscarPlanes() {
		listaPlanes = pl.obtenerPlanes();
		return SUCCESS;
	}

	
	// obtiene numero aleatorio para el afiliado
	private int obtenerNumero() {
		int x;
		do {
			x = (int) (Math.random() * 99999 + 1);
		} while (this.afi.busquedaPorCodigo(x) != null);
		return x;
	}


	public void generaCodigo() { //Parece que no se usa, lo dejo por si las moscas
		this.afiliadoCampo = obtenerNumero();
	}

}