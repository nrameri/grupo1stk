package com.softtek.academia.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="prestador")
public class Prestador {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id_prestador;
	private String nombre_completo;
	private int id_practica;
	private String direccion;
	private int localidad;
	private int telefono;

	//Constructor
	public Prestador() {
	}

	//Setters
	public void setId_prestador(int id_prestador) {
		this.id_prestador = id_prestador;
	}

	public void setNombre_completo(String nombre_completo) {
		this.nombre_completo = nombre_completo;
	}

	public void setId_practica(int id_practica) {
		this.id_practica = id_practica;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public void setLocalidad(int localidad) {
		this.localidad = localidad;
	}

	public void setTelefono(int telefono) {
		this.telefono = telefono;
	}

	//Getters
	public int getId_prestador() {
		return id_prestador;
	}

	public String getNombre_completo() {
		return nombre_completo;
	}

	public int getId_practica() {
		return id_practica;
	}

	public String getDireccion() {
		return direccion;
	}

	public int getLocalidad() {
		return localidad;
	}

	public int getTelefono() {
		return telefono;
	}

	@Override
	public String toString() {
		return "Prestador [id_prestador=" + id_prestador + ", nombre_completo=" + nombre_completo + ", id_practica="
				+ id_practica + ", direccion=" + direccion + ", localidad=" + localidad + ", telefono=" + telefono
				+ "]";
	}

	

}
