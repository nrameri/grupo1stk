package com.softtek.academia.entities;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="turno")
public class Turno {
	
	@Id
	private int id_turno;
	private Date fechaHora;
	
	@ManyToOne(optional=false)
	@JoinColumn(name= "AFILIADO")
	private Afiliado afiliado;
	
	@ManyToOne(optional=false)
	@JoinColumn(name= "PRESTADOR")
	private Prestador prestador;
	
	@ManyToOne(optional=false)
	@JoinColumn(name= "PRACTICA")
	private Practica practica;
	
	private float importe;
	private int duracion; //cantidad de minutos
	
	//Constructor
	public Turno(){}

	public Turno(int id_turno, Date fechaHora, Afiliado afiliado, Prestador prestador, Practica practica, float importe,
			int duracion) {
		super();
		this.id_turno = id_turno;
		this.fechaHora = fechaHora;
		this.afiliado = afiliado;
		this.prestador = prestador;
		this.practica = practica;
		this.importe = importe;
		this.duracion = duracion;
	}

	//Getters y setters
	public int getId_turno() {
		return id_turno;
	}

	public void setId_turno(int id_turno) {
		this.id_turno = id_turno;
	}

	public Date getFechaHora() {
		return fechaHora;
	}

	public void setFechaHora(Date fechaHora) {
		this.fechaHora = fechaHora;
	}

	public Afiliado getAfiliado() {
		return afiliado;
	}

	public void setAfiliado(Afiliado afiliado) {
		this.afiliado = afiliado;
	}

	public Prestador getPrestador() {
		return prestador;
	}

	public void setPrestador(Prestador prestador) {
		this.prestador = prestador;
	}

	public Practica getPractica() {
		return practica;
	}

	public void setPractica(Practica practica) {
		this.practica = practica;
	}

	public float getImporte() {
		return importe;
	}

	public void setImporte(float importe) {
		this.importe = importe;
	}

	public int getDuracion() {
		return duracion;
	}

	public void setDuracion(int duracion) {
		this.duracion = duracion;
	}
	

	@Override
	public String toString() {
		return "Turno [id_turno=" + id_turno + ", fechaHora=" + fechaHora + ", afiliado=" + afiliado + ", prestador="
				+ prestador + ", practica=" + practica + ", importe=" + importe + ", duracion=" + duracion + "]";
	}


	
	
}
