package com.softtek.academia.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="practica")
public class Practica {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id_practica;
	private String nombrepractica;
	

	
	//Constructor
	public Practica(){}

	//Setters
	public void setId_practica(int id_practica) {
		this.id_practica = id_practica;
	}

	public void setNombrepractica(String nombrepractica) {
		this.nombrepractica = nombrepractica;
	}
	


	//Getters
	public int getId_practica() {
		return id_practica;
	}

	public String getNombrepractica() {
		return nombrepractica;
	}

	@Override
	public String toString() {
		return "Practica [id_practica=" + id_practica + ", nombrepractica=" + nombrepractica + "]";
	}


}
