package com.softtek.academia.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="localidad")
public class Localidad {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id_localidad;
	private String nombre_loc;
	
	//Constructor
	public Localidad(){}

	//Setters
	public void setId_localidad(int id_localidad) {
		this.id_localidad = id_localidad;
	}

	public void setNombre_loc(String nombre_loc) {
		this.nombre_loc = nombre_loc;
	}

	//Getters
	public int getId_localidad() {
		return id_localidad;
	}

	public String getNombre_loc() {
		return nombre_loc;
	}

	@Override
	public String toString() {
		return "Localidad [id_localidad=" + id_localidad + ", nombre_loc=" + nombre_loc + "]";
	}
	
	
}
