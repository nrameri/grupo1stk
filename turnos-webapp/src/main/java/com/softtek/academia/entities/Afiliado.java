package com.softtek.academia.entities;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "afiliado")
public class Afiliado {

	@Id
	private int nro_afiliado;

	@ManyToOne(optional = false)
	@JoinColumn(name = "ID_PLAN")
	private Plan plan;

	private String nombre_completo;
	private String documento;
	private String sexo;
	private Date fecha_nacimiento;
	private String direccion;
	private int telefono;
	private String mail;
	private String estado_civil;
	private int familar_a_cargo;
	private String tipodocu;
	private String estado;

	// Constructores
	public Afiliado() {
	}

	public Afiliado(int nro_afiliado, Plan id_plan, String nombre_completo, String documento, String sexo,
			Date fecha_nacimiento, String direccion, int telefono, String mail, String estado_civil,
			int familiar_a_cargo, String tipodocu, String estado) {
		super();
		this.nro_afiliado = nro_afiliado;
		this.plan = id_plan;
		this.nombre_completo = nombre_completo;
		this.documento = documento;
		this.sexo = sexo;
		this.fecha_nacimiento = fecha_nacimiento;
		this.direccion = direccion;
		this.telefono = telefono;
		this.mail = mail;
		this.estado_civil = estado_civil;
		this.familar_a_cargo = familiar_a_cargo;
		this.tipodocu = tipodocu;
		this.estado = estado;
	}

	// Setters
	public void setNro_afiliado(int nro_afiliado) {
		this.nro_afiliado = nro_afiliado;
	}

	public void setPlan(Plan plan) {
		this.plan = plan;
	}

	public void setNombre_completo(String nombre_completo) {
		this.nombre_completo = nombre_completo;
	}

	public void setDocumento(String documento) {
		this.documento = documento;
	}

	public void setSexo(String sexo) {
		this.sexo = sexo;
	}

	public void setFecha_nacimiento(Date fecha_nacimiento) {
		this.fecha_nacimiento = fecha_nacimiento;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public void setTelefono(int telefono) {
		this.telefono = telefono;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public void setEstado_civil(String estado_civil) {
		this.estado_civil = estado_civil;
	}

	public void setFamiliar_a_cargo(int familiar_a_cargo) {
		this.familar_a_cargo = familiar_a_cargo;
	}

	public void setTipodocu(String tipodocu) {
		this.tipodocu = tipodocu;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	// Getters
	public int getNro_afiliado() {
		return nro_afiliado;
	}

	public Plan getPlan() {
		return plan;
	}

	public String getNombre_completo() {
		return nombre_completo;
	}

	public String getDocumento() {
		return documento;
	}

	public String getSexo() {
		return sexo;
	}

	public Date getFecha_nacimiento() {
		return fecha_nacimiento;
	}

	public String getDireccion() {
		return direccion;
	}

	public int getTelefono() {
		return telefono;
	}

	public String getMail() {
		return mail;
	}

	public String getEstado_civil() {
		return estado_civil;
	}

	public int getFamiliar_a_cargo() {
		return familar_a_cargo;
	}

	public String getTipodocu() {
		return tipodocu;
	}

	public String getEstado() {
		return estado;
	}

	@Override
	public String toString() {
		return "Afiliado [nro_afiliado=" + nro_afiliado + ", plan=" + plan + ", nombre_completo=" + nombre_completo
				+ ", documento=" + documento + ", sexo=" + sexo + ", fecha_nacimiento=" + fecha_nacimiento
				+ ", direccion=" + direccion + ", telefono=" + telefono + ", mail=" + mail + ", estado_civil="
				+ estado_civil + ", familiar_a_cargo=" + familar_a_cargo + ", tipodocu=" + tipodocu + ", estado="
				+ estado + "]";
	}

}
