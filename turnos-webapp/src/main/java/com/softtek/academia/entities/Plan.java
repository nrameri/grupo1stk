package com.softtek.academia.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="plan")
public class Plan {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id_plan;
	private String nombreplan;

	//Constructor
	public Plan(){}
	public Plan(int x){}
	//Setters
	public void setId_plan(int id_plan) {
		this.id_plan = id_plan;
	}

	public void setNombreplan(String nombreplan) {
		this.nombreplan = nombreplan;
	}

	//Getters
	public int getId_plan() {
		return id_plan;
	}
	public String getNombreplan() {
		return nombreplan;
	}

	@Override
	public String toString() {
		return "Plan [id_plan=" + id_plan + ", nombreplan=" + nombreplan + "]";
	}
	
	
}
