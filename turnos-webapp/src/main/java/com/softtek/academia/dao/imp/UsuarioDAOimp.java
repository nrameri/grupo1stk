package com.softtek.academia.dao.imp;

import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.orm.hibernate4.support.HibernateDaoSupport;

import com.softtek.academia.dao.UsuarioDAO;
import com.softtek.academia.entities.Usuario;

public class UsuarioDAOimp extends HibernateDaoSupport implements UsuarioDAO {

	private List<Usuario>listadoUser;
	private int access;
	private boolean registro;
	
	//Setters y Getters
	public void setAccess(int acceso) {
		this.access = acceso;
	}
	public int getAccess() {
		return access;
	}
	public void setRegistro(boolean registro) {
		this.registro = registro;
	}
	public boolean getRegistro() {
		return registro;
	}
	
	//IMPLEMENTACIONES
	
	//Alta de usuario
	public boolean alta(Usuario u) {
		try {
			getHibernateTemplate().persist(u);
			registro = true;
		}catch (DataAccessException ex) {
			registro = false;
		}
		return registro;
	}
	
	@Override //Baja logica de usuario
	public boolean eliminar(Usuario u) {
		try {
			getHibernateTemplate().update(u);
			registro = true;
		}catch (DataAccessException ex) {
			registro = false;
		}
		return registro;
	}
	
	@Override //Modificacion de usuario
	public boolean modificar(Usuario u) {
		try {
			getHibernateTemplate().update(u);
			registro = true;
		}catch (DataAccessException ex) {
			registro = false;
		}
		return registro;
	}

	@Override //Muestra todos los usuarios(solo Admin)
	public List<Usuario> findAll() {
		try{
			listadoUser = (List<Usuario>) getHibernateTemplate().find("from Usuario");
		}catch(DataAccessException ex){
			listadoUser = null;
		}
		return listadoUser;
	}

	//Busca por nombre y password (usado en Login)
	public int findByName(String name, String pass) {
		int accede = 0;
		int x = obtenerID(name);
		try {
			Usuario u = getHibernateTemplate().get(Usuario.class, x);
			if( u != null) {
				if(u.getPass().equals(pass)) {
					accede = u.getNivel();
					//System.out.println("Acceso concedido. Usuario: " + name + ", nivel de acceso: " + accede);
				}else {
					accede = 0;
				}
			}else {
				accede = 0;
			}
		}catch (DataAccessException ex) {
			System.out.println("Error en el DAO al ejecutar consulta");
			ex.printStackTrace();
		}
		return accede;
	}
	
	/**
	 * metodo utilitario para acceder al ID del Usuario
	 * @param name como el nombre de usuario a buscar
	 * @return ID de usuario
	 */
	private int obtenerID(String name) {
		int identificador = 0;
		listadoUser = findAll();
		for(Usuario u: listadoUser) {
			if(u.getNombre().equals(name)) {
				identificador = u.getId_usuario();
			}
		}
		return identificador;
	}
	
}
