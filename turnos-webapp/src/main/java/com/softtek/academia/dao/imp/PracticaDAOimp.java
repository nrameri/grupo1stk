package com.softtek.academia.dao.imp;

import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.orm.hibernate4.support.HibernateDaoSupport;

import com.softtek.academia.dao.PracticaDAO;
import com.softtek.academia.entities.Afiliado;
import com.softtek.academia.entities.Practica;

public class PracticaDAOimp extends HibernateDaoSupport implements PracticaDAO{
	
	private Practica practica;
	private List<Practica>listadoPractica;
	
	//Constructor vacio
	public PracticaDAOimp(){}
	
	//GETTERS Y SETTERS
	public Practica getPractica() {
		return practica;
	}
	public void setPractica(Practica practica) {
		this.practica = practica;
	}
	public List<Practica> getListadoPractica() {
		return listadoPractica;
	}
	public void setListadoPractica(List<Practica> listadoPractica) {
		this.listadoPractica = listadoPractica;
	}
	
	//BUSCA TODAS LAS PRACTICAS
	public List<Practica> findAll(){
		try {
			listadoPractica = (List<Practica>) getHibernateTemplate().find("from Practica");
		}catch (DataAccessException ex) {
			System.out.println("Error al obtener listado de Practica");
		}
		return listadoPractica;
	}


}
