package com.softtek.academia.dao.imp;

import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.orm.hibernate4.support.HibernateDaoSupport;

import com.softtek.academia.dao.PrestadorDAO;
import com.softtek.academia.entities.Afiliado;
import com.softtek.academia.entities.Prestador;

public class PrestadorDAOimp extends HibernateDaoSupport implements PrestadorDAO{
	private Prestador prestador;
	private List<Prestador>listado;
	
	//Constructor
	public PrestadorDAOimp(){};
	
	//GETTERS Y SETTERS
	public Prestador getPrestador() {
		return prestador;
	}
	public void setPrestador(Prestador prestador) {
		this.prestador = prestador;
	}
	public List<Prestador> getListado() {
		return listado;
	}
	public void setListado(List<Prestador> listado) {
		this.listado = listado;
	}

	//METODO PARA BUSCAR PRESTADORES
	public List<Prestador> findAll(){
		try {
			listado = (List<Prestador>) getHibernateTemplate().find("from Prestador");
		}catch (DataAccessException ex) {
			System.out.println("Error al obtener listado de prestadores");
		}
		return listado;
	}


}