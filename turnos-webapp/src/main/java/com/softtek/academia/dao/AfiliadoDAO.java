package com.softtek.academia.dao;

import java.util.List;
import com.softtek.academia.entities.Afiliado;

public interface AfiliadoDAO {

	List<Afiliado> findAll();
	List<Afiliado> findByName(String parametroBusqueda);
	boolean alta(Afiliado afiliado);
	boolean eliminar(Afiliado afiliado);
	boolean modificar(Afiliado afiliado);
	Afiliado findByID(int x);
}
