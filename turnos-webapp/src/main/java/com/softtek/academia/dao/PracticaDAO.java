package com.softtek.academia.dao;

import java.util.List;

import com.softtek.academia.entities.Practica;

public interface PracticaDAO {

	List<Practica> findAll();
}
