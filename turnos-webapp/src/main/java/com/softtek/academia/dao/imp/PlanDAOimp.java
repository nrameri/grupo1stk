package com.softtek.academia.dao.imp;

import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.orm.hibernate4.support.HibernateDaoSupport;

import com.softtek.academia.dao.PlanDAO;
import com.softtek.academia.entities.Plan;

public class PlanDAOimp extends HibernateDaoSupport implements PlanDAO{

	private List<Plan> listado;

	public List<Plan> findAll() {
		try {
			listado = (List<Plan>) getHibernateTemplate().find("from Plan");
		} catch (DataAccessException ex) {
			System.out.println("Error al obtener listado de planes");
		}
		return listado;
	}


}
