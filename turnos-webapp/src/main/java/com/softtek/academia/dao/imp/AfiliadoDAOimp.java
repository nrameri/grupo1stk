package com.softtek.academia.dao.imp;

import java.util.List;
import org.springframework.orm.hibernate4.HibernateTemplate;

import org.springframework.dao.DataAccessException;
import org.springframework.orm.hibernate4.support.HibernateDaoSupport;

import com.softtek.academia.dao.AfiliadoDAO;
import com.softtek.academia.entities.Afiliado;

public class AfiliadoDAOimp extends HibernateDaoSupport implements AfiliadoDAO {

	private Afiliado afiliado;
	private List<Afiliado> listadoAfi;
	private boolean registro;

	// CONSTRUCTOR VACIO
	public AfiliadoDAOimp() {
	}

	// GETTERS Y SETTERS
	public Afiliado getAfiliado() {
		return afiliado;
	}

	public void setAfiliado(Afiliado afiliado) {
		this.afiliado = afiliado;
	}

	// BUSQUEDA LISTADO TOTAL DE AFILIADOS
	public List<Afiliado> findAll() {
		try {
			listadoAfi = (List<Afiliado>) getHibernateTemplate().find("from Afiliado");
		} catch (DataAccessException ex) {
			listadoAfi = null;
		}
		return listadoAfi;
	}

	// BUSQUEDA POR PARAMETRO
	public List<Afiliado> findByName(String parametroBusqueda) {
		String QUERY = "from Afiliado where (NOMBRE_COMPLETO like '%'parametroBusqueda'%')OR(DOCUMENTO like '%'parametroBusqueda'%')OR(ID_PLAN like '%'parametroBusqueda'%')OR(NRO_AFILIADO like '%'parametroBusqueda'%')";
		try {
			listadoAfi = (List<Afiliado>) getHibernateTemplate().find(QUERY);
			return listadoAfi;
		} catch (DataAccessException ex) {
			listadoAfi = null;
		}
		return listadoAfi;
	}

	// BUSQUEDA AFILIADO por id
	@Override
	public Afiliado findByID(int x) {
		try {
			afiliado = (Afiliado) getHibernateTemplate().get(Afiliado.class, x);
		} catch (Exception e) {
			System.out.println("No busca al afiliado por id");
		}
		return afiliado;
	}

	// ALTA AFILIADO
	public boolean alta(Afiliado afiliado) {
		// System.out.println(afiliado + " DAo");
		try {
			getHibernateTemplate().save(afiliado);
			registro = true;
		} catch (DataAccessException ex) {
			registro = false;
			// System.out.println(registro + "DAO");
		}
		return registro;
	}

	// BAJA AFILIADO
	public boolean eliminar(Afiliado afiliado) {
		// System.out.println(afiliado + "DAO");
		try {
			HibernateTemplate hibernateTemplate = getHibernateTemplate();
			hibernateTemplate.delete(afiliado);
			registro = true;
			// System.out.println("Se elimino bien por el dao");

		} catch (Exception ex) {
			System.out.println(ex);
			registro = false;
		}
		return registro;
	}

	// MODIFICACION AFILIADO
	public boolean modificar(Afiliado afiliado) {
		try {
			getHibernateTemplate().update(afiliado);
			registro = true;
			System.out.println(registro + " DAO");
		} catch (DataAccessException ex) {
			registro = false;
		}
		return registro;
	}

}
