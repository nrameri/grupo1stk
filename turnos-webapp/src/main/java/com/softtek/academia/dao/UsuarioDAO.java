package com.softtek.academia.dao;

import java.util.List;

import com.softtek.academia.entities.Usuario;

public interface UsuarioDAO {
	boolean alta(Usuario u);
	boolean eliminar(Usuario u);
	boolean modificar(Usuario u);
	List<Usuario> findAll();
	int findByName(String name, String pass);
}
