package com.softtek.academia.dao;

import java.util.List;

import com.softtek.academia.entities.Plan;

public interface PlanDAO {

	List<Plan> findAll();
}
