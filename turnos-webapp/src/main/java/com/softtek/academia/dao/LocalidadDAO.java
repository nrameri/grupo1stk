package com.softtek.academia.dao;

import java.util.List;

import com.softtek.academia.entities.Localidad;

public interface LocalidadDAO {
	List<Localidad> findAll();
}
