package com.softtek.academia.dao;

import java.util.List;

import com.softtek.academia.entities.Afiliado;
import com.softtek.academia.entities.Prestador;
import com.softtek.academia.entities.Turno;

public interface TurnoDAO {

	List<Turno> findAll();
	List<Turno> findByPrestador(Prestador p);
	List<Turno> findByAfiliado(Afiliado af);
	
	boolean altaTurno(Turno t);
	boolean eliminarTurno(Turno unTurno);
	boolean modificarTurno(Turno t);
	Turno findByID(int x);
}
