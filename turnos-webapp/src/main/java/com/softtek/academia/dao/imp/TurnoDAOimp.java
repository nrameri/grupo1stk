package com.softtek.academia.dao.imp;

import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.orm.hibernate4.support.HibernateDaoSupport;

import com.softtek.academia.dao.TurnoDAO;
import com.softtek.academia.entities.Afiliado;
import com.softtek.academia.entities.Prestador;
import com.softtek.academia.entities.Turno;

public class TurnoDAOimp extends HibernateDaoSupport implements TurnoDAO{
	private List<Turno> listado;
	private Turno turno;
	private boolean gestion;
	private boolean registroT;


//GETTERS Y SETERS
	public List<Turno> getListado() {
		return listado;
	}
	public void setListado(List<Turno> listado) {
		this.listado = listado;
	}
	public Turno getTurno() {
		return turno;
	}
	public void setTurno(Turno turno) {
		this.turno = turno;
	}
	public boolean isGestion() {
		return gestion;
	}
	public void setGestion(boolean gestion) {
		this.gestion = gestion;
	}
	

	// devuelve todos los turnos
	public List<Turno> findAll() {
		try {
			listado = (List<Turno>) getHibernateTemplate().find("from Turno");
			//System.out.println(listado + "pasa por el dao");
		} catch (DataAccessException e) {
			listado = null;;
		}
		return listado;
	}
		
	// devuelve todos los turnos del prestador
	@Override
	public List<Turno> findByPrestador(Prestador p) {
		String query = "from Turno where prestador = " + p.getId_prestador();
		try {
			listado = (List<Turno>) getHibernateTemplate().find(query);
		} catch (Exception e) {

		}
		return listado;
	}
	
	// devuelve turnos por ID
		@Override
		public Turno findByID(int x) {
				try {
					turno = (Turno) getHibernateTemplate().get(Turno.class, x);
				} catch (Exception e) {
					System.out.println("No busca turno por id");
				}
			return turno;
		}
		
	
	// devuelve todos los turnos del afiliado
	public List<Turno> findByAfiliado(Afiliado af) {
		String query = "from Turno where afiliado = " + af.getNro_afiliado();
		try {
			listado = (List<Turno>) getHibernateTemplate().find(query);
		} catch (Exception e) {
			
		}
		return listado;
	}
	
	//ALTA TURNO
		public boolean altaTurno(Turno turno){
			System.out.println(turno + " DAo");
			try{
				getHibernateTemplate().save(turno);
				registroT = true;
			} catch (DataAccessException ex) {
				registroT = false;
				System.out.println("No crea turno" + "DAO");
			}
			return registroT;
		}
		
		// BAJA TURNO
		public boolean eliminarTurno(Turno unTurno) {
			System.out.println(unTurno + "DAO");
			try {
				HibernateTemplate hibernateTemplate = getHibernateTemplate();
				hibernateTemplate.delete(unTurno);
				registroT = true;
				System.out.println("Se elimino bien por el dao");

			} catch (Exception ex) {
				System.out.println(ex);
				registroT = false;
			}
			return registroT;
		}
		
		//MODIFICACION AFILIADO
		public boolean modificarTurno(Turno turno){
			try{
				getHibernateTemplate().update(turno);
				registroT = true;
			}catch(DataAccessException ex){
				registroT = false;
			}
			return registroT;
		}
}		
	
/*	// gestiona el alta de nuevo turno
	public boolean alta(Turno t) {
		try {
			getHibernateTemplate().save(t); 
			gestion = true;
		}catch (Exception e) {
			gestion = false;
		}
		return gestion;
	}

	// gestiona la cancelacion de turno
	public boolean eliminar(Turno t) {
		try {
			getHibernateTemplate().update(t); 
			gestion = true;
		}catch (Exception e) {
			gestion = false;
		}
		return gestion;
	}

	// gestiona el alta de nuevo turno
	public boolean modificar(Turno t) {
		try {
			getHibernateTemplate().update(t); 
			gestion = true;
		}catch (Exception e) {
			gestion = false;
		}
		return gestion;
	}*/

