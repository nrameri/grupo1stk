package com.softtek.academia.dao;

import java.util.List;

import com.softtek.academia.entities.Prestador;

public interface PrestadorDAO {

	List<Prestador> findAll();
}
