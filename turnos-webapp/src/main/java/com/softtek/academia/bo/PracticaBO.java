package com.softtek.academia.bo;

import java.util.List;


import com.softtek.academia.entities.Practica;

public interface PracticaBO {
	List<Practica> obtenerPracticas();
}
