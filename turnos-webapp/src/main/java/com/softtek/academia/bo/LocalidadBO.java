package com.softtek.academia.bo;

import java.util.List;

import com.softtek.academia.entities.Localidad;

public interface LocalidadBO {
	List<Localidad> obtenerLocalidades();
}
