package com.softtek.academia.bo.Impl;

import java.util.List;

import com.softtek.academia.bo.PrestadorBO;
import com.softtek.academia.dao.PrestadorDAO;
import com.softtek.academia.entities.Practica;
import com.softtek.academia.entities.Prestador;

public class PrestadorBOimp implements PrestadorBO{
	PrestadorDAO prestadorDAO;
	List<Prestador>listaPrestadores;

	//Constructor 
	public PrestadorBOimp (){}
	
	//GETTER Y SETTER
	public PrestadorDAO getPrestadorDAO() {
		return prestadorDAO;
	}
	public void setPrestadorDAO(PrestadorDAO prestadorDAO) {
		this.prestadorDAO = prestadorDAO;
	}
	public List<Prestador> getListaPrestadores() {
		return listaPrestadores;
	}
	public void setListaPrestadores(List<Prestador> listaPrestadores) {
		this.listaPrestadores = listaPrestadores;
	}
	
	//BUSQUEDA LISTA
	public List<Prestador> obtenerPrestadores() {
		listaPrestadores= prestadorDAO.findAll();
		return listaPrestadores;
	}
	

}
