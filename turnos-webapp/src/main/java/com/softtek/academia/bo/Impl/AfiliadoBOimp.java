package com.softtek.academia.bo.Impl;

import java.util.Date;
import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.softtek.academia.bo.AfiliadoBO;
import com.softtek.academia.dao.AfiliadoDAO;
import com.softtek.academia.dao.PlanDAO;
import com.softtek.academia.entities.Afiliado;
import com.softtek.academia.entities.Plan;
import org.springframework.transaction.annotation.Transactional;

public class AfiliadoBOimp implements AfiliadoBO {

	private AfiliadoDAO afiliado;
	private List<Afiliado> listaAfiliados;
	private Afiliado af;
	private boolean registro;

	// Constructor
	public AfiliadoBOimp() {
	}

	// Getters y setters
	public AfiliadoDAO getAfiliado() {
		return afiliado;
	}

	public void setAfiliado(AfiliadoDAO afiliado) {
		this.afiliado = afiliado;
	}

	public Afiliado getAf() {
		return af;
	}

	public void setAf(Afiliado af) {
		this.af = af;
	}

	public List<Afiliado> getListaAfiliados() {
		return listaAfiliados;
	}

	public void setListaAfiliados(List<Afiliado> listaAfiliados) {
		this.listaAfiliados = listaAfiliados;
	}

	public boolean isRegistro() {
		return registro;
	}

	public void setRegistro(boolean registro) {
		this.registro = registro;
	}

	// Metodos
	@Override
	public List<Afiliado> obtenerAfiliadoGeneral() {
		listaAfiliados = afiliado.findAll();
		return listaAfiliados;
	}

	@Override
	public List<Afiliado> obtenerAfiliadoPorBusqueda(String parametroBusqueda) {
		listaAfiliados = afiliado.findByName(parametroBusqueda);
		return listaAfiliados;
	}

	public Afiliado busquedaPorCodigo(int codigo) {
		this.af = afiliado.findByID(codigo);
		return af;
	}

	// ALTA DE AFILIADO
	@Transactional
	public boolean addAfiliado(Afiliado afiliado) {
		boolean registro = this.afiliado.alta(afiliado);
		return registro;
	}

	@Transactional
	public boolean eliminarAfiliado(Afiliado afiliadoeliminar) {
		registro = afiliado.eliminar(afiliadoeliminar);
		return registro;

	}

	// MODIFICAR AFILIADO
	@Transactional
	public boolean modificarAfiliado(Afiliado afiliado) { // NO MODIFICA AUNQUE DAO DEVUELVE TRUE
		System.out.println(afiliado.toString() + " bo");
		boolean registro = this.afiliado.modificar(afiliado);
		System.out.println(registro + " BO");
		return registro;
	}

}
