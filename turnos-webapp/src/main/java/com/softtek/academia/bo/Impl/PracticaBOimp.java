package com.softtek.academia.bo.Impl;

import java.util.List;

import com.softtek.academia.bo.PracticaBO;
import com.softtek.academia.dao.PracticaDAO;
import com.softtek.academia.entities.Practica;

public class PracticaBOimp implements PracticaBO{
	PracticaDAO practicaDAO;
	List<Practica>listaPracticas;
	
	//Constructor
	public PracticaBOimp(){}
	
	//GETTERS Y SETTERS
	public PracticaDAO getPracticaDAO() {
		return practicaDAO;
	}
	public void setPracticaDAO(PracticaDAO practicaDAO) {
		this.practicaDAO = practicaDAO;
	}
	public List<Practica> getListaPracticas() {
		return listaPracticas;
	}
	public void setListaPracticas(List<Practica> listaPracticas) {
		this.listaPracticas = listaPracticas;
	}
	
	//BUSQUEDA LISTA
	@Override
	public List<Practica> obtenerPracticas() {
	listaPracticas = practicaDAO.findAll();
		return listaPracticas;
	}

}
