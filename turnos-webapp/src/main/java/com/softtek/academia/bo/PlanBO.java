package com.softtek.academia.bo;

import java.util.List;

import com.softtek.academia.entities.Plan;

public interface PlanBO {
	List<Plan> obtenerPlanes();
	
}
