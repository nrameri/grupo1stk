package com.softtek.academia.bo.Impl;

import com.softtek.academia.bo.LoginBO;
import com.softtek.academia.dao.UsuarioDAO;

public class LoginBOimp implements LoginBO {
	
	private UsuarioDAO usuario;
	private boolean access;
	
	
	public UsuarioDAO getUsuario() {
		return usuario;
	}

	public void setUsuario(UsuarioDAO usuario) {
		this.usuario = usuario;
	}

	public void setAccess(boolean access) {
		this.access = access;
	}
	
	@Override
	public boolean acceder(String user, String pas) {
		if(usuario.findByName(user, pas) != 0) {
			this.access = true;
		}
		return access;
	}

}
