package com.softtek.academia.bo;

import java.util.List;

import com.softtek.academia.entities.Prestador;


public interface PrestadorBO {
	List<Prestador> obtenerPrestadores();
}
