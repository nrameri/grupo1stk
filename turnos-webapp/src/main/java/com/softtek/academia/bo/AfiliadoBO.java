package com.softtek.academia.bo;

import java.util.List;

import com.softtek.academia.entities.Afiliado;

public interface AfiliadoBO {

	List<Afiliado> obtenerAfiliadoGeneral();

	List<Afiliado> obtenerAfiliadoPorBusqueda(String parametroBusqueda);

	Afiliado busquedaPorCodigo(int codigo);

	boolean addAfiliado(Afiliado afiliado);

	boolean modificarAfiliado(Afiliado afiliado);

	boolean eliminarAfiliado(Afiliado afiliado);

}
