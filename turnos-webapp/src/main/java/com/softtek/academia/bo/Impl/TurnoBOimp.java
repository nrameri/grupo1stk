package com.softtek.academia.bo.Impl;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.softtek.academia.bo.TurnoBO;
import com.softtek.academia.dao.TurnoDAO;
import com.softtek.academia.entities.Afiliado;
import com.softtek.academia.entities.Practica;
import com.softtek.academia.entities.Prestador;
import com.softtek.academia.entities.Turno;

public class TurnoBOimp implements TurnoBO{

	private boolean gestion;
	private Turno turno;
	private TurnoDAO turnoDAO;
	private List<Turno>listaTurnos;
	
	//GETTERS Y SETTERS

	public boolean isGestion() {
		return gestion;
	}
	public void setGestion(boolean gestion) {
		this.gestion = gestion;
	}
	public Turno getTurno() {
		return turno;
	}
	public void setTurno(Turno turno) {
		this.turno = turno;
	}
	public TurnoDAO getTurnoDAO() {
		return turnoDAO;
	}
	public void setTurnoDAO(TurnoDAO turnoDAO) {
		this.turnoDAO = turnoDAO;
	}
	public List<Turno> getListaTurnos() {
		return listaTurnos;
	}
	public void setListaTurnos(List<Turno> listaTurnos) {
		this.listaTurnos = listaTurnos;
	}
	
	//BUSCA LA LISTA DE TURNOS
	@Override
	public List<Turno> obtenerTurnos() {
		listaTurnos= turnoDAO.findAll();
		return listaTurnos;
	}
	public Turno busquedaPorCodigo(int codigo) {
		this.turno = turnoDAO.findByID(codigo);
		return turno;
	}
	
	//CREACION MODIFICACION Y ELMINACION TURNOS
	@Transactional
	public boolean crearTurno(Turno t){
		boolean registro = turnoDAO.altaTurno(t);
		return registro;
	}
	
	@Override
	public boolean modificarTurno(int nroTurno, Date time, Afiliado afi, Prestador presta, Practica practica,
			float coste, int lapso) {
		turno = new Turno (nroTurno, time, afi, presta, practica, coste, lapso);
		turnoDAO.modificarTurno(turno);
		return gestion;
	}
	
	//ELIMINAR TURNO 
	@Transactional
	@Override
	 public boolean eliminarTurno(Turno unTurno){
		 System.out.println(unTurno); 
	boolean	registro = turnoDAO.eliminarTurno(unTurno);
	 System.out.println("Se elimino turno bo");
	    return registro;

	  }

}
