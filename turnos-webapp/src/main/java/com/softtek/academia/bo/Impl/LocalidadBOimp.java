package com.softtek.academia.bo.Impl;

import java.util.List;

import com.softtek.academia.bo.LocalidadBO;
import com.softtek.academia.dao.LocalidadDAO;
import com.softtek.academia.entities.Localidad;

public class LocalidadBOimp implements LocalidadBO {
	private LocalidadDAO localidad;
	private List<Localidad>listado;

	public LocalidadDAO getLocalidad() {
		return localidad;
	}
	public void setLocalidad(LocalidadDAO localidad) {
		this.localidad = localidad;
	}
	
	@Override
	public List<Localidad> obtenerLocalidades() {
		this.listado = this.localidad.findAll();
		return listado;
	}
	
	
}
