package com.softtek.academia.bo;

import java.util.Date;
import java.util.List;

import com.softtek.academia.entities.Afiliado;
import com.softtek.academia.entities.Plan;
import com.softtek.academia.entities.Practica;
import com.softtek.academia.entities.Prestador;
import com.softtek.academia.entities.Turno;

public interface TurnoBO {
	List<Turno> obtenerTurnos();
	Turno busquedaPorCodigo(int codigo);
	
	boolean crearTurno(Turno t);
	boolean modificarTurno(int nroTurno, Date time, Afiliado afi, Prestador presta, Practica practica, float coste, int lapso);
	boolean eliminarTurno (Turno unTurno);
	
}
