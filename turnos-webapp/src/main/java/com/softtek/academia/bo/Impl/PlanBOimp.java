package com.softtek.academia.bo.Impl;

import java.util.List;

import com.softtek.academia.bo.PlanBO;
import com.softtek.academia.dao.PlanDAO;
import com.softtek.academia.entities.Plan;

public class PlanBOimp implements PlanBO {

	private PlanDAO plan;
	private List<Plan> listado;
	
	//Constructor
	public PlanBOimp() {
	}
	
	//GETTER Y SETTER
	public PlanDAO getPlan() {
		return plan;
	}
	public void setPlan(PlanDAO plan) {
		this.plan = plan;
	}
	public List<Plan> getListado() {
		return listado;
	}
	public void setListado(List<Plan> listado) {
		this.listado = listado;
	}

	//METODO BUSQUEDA
	@Override
	public List<Plan> obtenerPlanes() {
		try {
			listado = plan.findAll();
		}catch (Exception e) {
			System.out.println("Error en listado desde BO");
		}
		return listado;
	}

}
